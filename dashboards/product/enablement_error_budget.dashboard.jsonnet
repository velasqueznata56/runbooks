// This file is autogenerated using scripts/update_stage_error_budget_dashboards.rb
// Please feel free to customize this file.
local errorBudgetsDashboards = import './error_budget_dashboards.libsonnet';

errorBudgetsDashboards
.dashboard('enablement', groups=['database', 'geo', 'global_search'])
.trailer()
